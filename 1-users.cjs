const users = {
    John: {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland",
    },
    Ron: {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK",
    },
    Wanda: {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany",
    },
    Rob: {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA",
    },
    Pike: {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany",
    }
};

// Q1 Find all users who are interested in playing video games.

function videoGamers(data){
    return Object.keys(data).filter((user)=>{
        if(data[user].interests[0].includes('Video Games')){
            return user;
        }
    })
}
console.log(videoGamers(users));

// Q2 Find all users staying in Germany.

function GermanResident(data){
    return Object.keys(data).filter((user)=>{
        if(data[user].nationality.includes('Germany')){
            return user;
        }
    })
}

console.log(GermanResident(users));

// Q3 Sort users based on their seniority level
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10


// Q4 Find all users with masters Degree.

function masterDegree(data){
    return Object.keys(data).filter((user)=>{
        if(data[user].qualification.includes('Masters')){
            return user;
        }
    })
}

console.log(masterDegree(users));

// Q5 Group users based on their Programming language mentioned in their designation.

function ProgrammingLang(data){
    // let languages = {"Golang":[],"Python":[],"Javascript":[]};
    // console.log(Object.keys(languages).map((input)=>{return input}))
    // console.log("map",Object.keys(languages).map((element,index)=>{
    //     return languages[index];
    // }))
    return Object.keys(data).map((user)=>{
        if(data[user].desgination.includes(Object.keys(languages).map((input)=>{return input}))){
            console.log("ok")
            return user;
        }
    })
}

console.log(ProgrammingLang(users));
