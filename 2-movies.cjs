const favouritesMovies = {
    Matrix: {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M",
    },
    FightClub: {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M",
    },
    Inception: {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M",
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M",
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M",
    },
    Titanic: {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M",
    },
};

// Q1. Find all the movies with total earnings more than $500M.

function earnings500(data) {
    return Object.keys(data).reduce((acc, filmName) => {
        if (Number(data[filmName].totalEarnings.replace(/[$M]/g, "")) > 500) {
            acc[filmName] = data[filmName];
        }
        return acc;
    }, {});
}

console.log(earnings500(favouritesMovies));

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

function morethan3Nomine(data) {
    return Object.keys(data).reduce((acc, filmName) => {
        if (
            Number(data[filmName].totalEarnings.replace(/[$M]/g, "")) > 500 &&
            data[filmName].oscarNominations > 3
        ) {
            acc[filmName] = data[filmName];
        }
        return acc;
    }, {});
}

console.log(morethan3Nomine(favouritesMovies));

// Q.3 Find all movies of the actor "Leonardo Dicaprio".

function actorFinder(data, actorname) {
    return Object.keys(data).reduce((acc, filmName) => {
        data[filmName].actors.map((actor) => {
            if (actor == actorname) {
                acc[filmName] = data[filmName];
            }
        });
        return acc;
    }, {});
}

console.log(actorFinder(favouritesMovies, "Leonardo Dicaprio"));

// Q.4 Sort movies (based on IMDB rating)

function IMDBsorter(data) {
    let res = [];
    let i = 0;
    Object.keys(data).map((filmname1) => {
        let j = i;
        let acc = 0;
        Object.keys(data).map((filmname2) => {
            // if(filmname1 != filmname2){
            // if(data[filmname2].imdbRating == acc){
            //         if (Number(data[filmname2].totalEarnings.replace(/[$M]/g, "")) > Number(data[filmname1].totalEarnings.replace(/[$M]/g, ""))){
            //         acc = {filmname1 : data[filmname1]}
            //         }
            //         else{
            //         acc = {filmname2 : data[filmname2]}
            //         }
            //     }
            //     if(data[filmname2].imdbRating >data[filmname1].imdbRating){
            //         acc = {filmname1 : data[filmname1]}
            //     }
            //     else{
            //         acc = {filmname2 : data[filmname2]}
            //     }
            // }
            // res[acc]

            // console.log(filmname);
            // console.log(filmname2)
            // console.log(i,j)
            j++;
        });
        console.log(acc);

        i++;
    });
}

console.log(IMDBsorter(favouritesMovies));

//     if IMDB ratings are same, compare totalEarning as the secondary metric.
// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//     drama > sci-fi > adventure > thriller > crime
