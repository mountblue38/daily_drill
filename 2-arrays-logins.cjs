let data = [
    {
        id: 1,
        first_name: "Valera",
        last_name: "Pinsent",
        email: "vpinsent0@google.co.jp",
        gender: "Male",
        ip_address: "253.171.63.171",
    },
    {
        id: 2,
        first_name: "Kenneth",
        last_name: "Hinemoor",
        email: "khinemoor1@yellowbook.com",
        gender: "Polygender",
        ip_address: "50.231.58.150",
    },
    {
        id: 3,
        first_name: "Roman",
        last_name: "Sedcole",
        email: "rsedcole2@addtoany.com",
        gender: "Genderqueer",
        ip_address: "236.52.184.83",
    },
    {
        id: 4,
        first_name: "Lind",
        last_name: "Ladyman",
        email: "lladyman3@wordpress.org",
        gender: "Male",
        ip_address: "118.12.213.144",
    },
    {
        id: 5,
        first_name: "Jocelyne",
        last_name: "Casse",
        email: "jcasse4@ehow.com",
        gender: "Agender",
        ip_address: "176.202.254.113",
    },
    {
        id: 6,
        first_name: "Stafford",
        last_name: "Dandy",
        email: "sdandy5@exblog.jp",
        gender: "Female",
        ip_address: "111.139.161.143",
    },
    {
        id: 7,
        first_name: "Jeramey",
        last_name: "Sweetsur",
        email: "jsweetsur6@youtube.com",
        gender: "Genderqueer",
        ip_address: "196.247.246.106",
    },
    {
        id: 8,
        first_name: "Anna-diane",
        last_name: "Wingar",
        email: "awingar7@auda.org.au",
        gender: "Agender",
        ip_address: "148.229.65.98",
    },
    {
        id: 9,
        first_name: "Cherianne",
        last_name: "Rantoul",
        email: "crantoul8@craigslist.org",
        gender: "Genderfluid",
        ip_address: "141.40.134.234",
    },
    {
        id: 10,
        first_name: "Nico",
        last_name: "Dunstall",
        email: "ndunstall9@technorati.com",
        gender: "Female",
        ip_address: "37.12.213.144",
    },
];

// 1. Find all people who are Agender

function findAgenders(data) {
    let agenders = data.filter((element) => {
        return element.gender == "Agender";
    });
    return agenders;
}

console.log(findAgenders(data));

// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]

function ipSpliter(data) {
    data.map((element) => {
        element.SplitedIP = element.ip_address
            .split(".")
            .map((input) => {
                return Number(input);
            });
    });
    return data;
}

console.log(ipSpliter(data));

// 3. Find the sum of all the second components of the ip addresses.

function sumOfSecondComp(data) {
    let result = data.reduce((acc, curr) => {
        return acc + curr.SplitedIP[1];
    }, 0);
    return result;
}

console.log(sumOfSecondComp(data));

// 3. Find the sum of all the fourth components of the ip addresses.

function sumOfFourthComp(data) {
    let result = data.reduce((acc, curr) => {
        return acc + curr.SplitedIP[3];
    }, 0);
    return result;
}

console.log(sumOfFourthComp(data));

// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.

const computeFullname = function (data) {
    data.map((element) => {
        element.full_name =
            element.first_name + " " + element.last_name;
    });
    return data;
};

console.log(computeFullname(data));

// 5. Filter out all the .org emails

const filterOrgMails = function (data) {
    let result = data.filter((element) => {
        return element.email.includes(".org");
    });
    return result;
};
console.log(filterOrgMails(data));

// 6. Calculate how many .org, .au, .com emails are there

const emailCalc = function (data) {
    let result = { ".org": 0, ".au": 0, ".com": 0 };
    data.map((element) => {
        if (element.email.includes(".org")) {
            result[".org"] += 1;
        }
        if (element.email.includes(".au")) {
            result[".au"] += 1;
        }
        if (element.email.includes(".com")) {
            result[".com"] += 1;
        }
    });
    return result;
};

console.log(emailCalc(data));

// 7. Sort the data in descending order of first name

const sortedData = function (data) {
    function compare(curValue, nextValue) {
        if (curValue.first_name > nextValue.first_name) {
            return -1;
        }
        if (curValue.first_name < nextValue.first_name) {
            return 1;
        }
        return 0;
    }
    return data.sort(compare);
};

console.log(sortedData(data));
