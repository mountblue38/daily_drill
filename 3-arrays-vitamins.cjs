const items = [
    {
        name: "Orange",
        available: true,
        contains: "Vitamin C",
    },
    {
        name: "Mango",
        available: true,
        contains: "Vitamin K, Vitamin C",
    },
    {
        name: "Pineapple",
        available: true,
        contains: "Vitamin A",
    },
    {
        name: "Raspberry",
        available: false,
        contains: "Vitamin B, Vitamin A",
    },
    {
        name: "Grapes",
        contains: "Vitamin D",
        available: false,
    },
];

// 1. Get all items that are available

function availableItems(data) {
    if (!Array.isArray(data)) {
        return [];
    }
    return data.reduce((acc, curr) => {
        if (curr.available == true) {
            acc.push(curr);
        }
        return acc;
    }, []);
}
console.log(availableItems(items));

// 2. Get all items containing only Vitamin C.

function containsVitC(data, str) {
    if (!Array.isArray(data) || typeof str != "string") {
        return [];
    }
    return data.reduce((acc, curr) => {
        if (curr.contains.includes(str)) {
            acc.push(curr);
        }
        return acc;
    }, []);
}
console.log(containsVitC(items, "Vitamin C"));

// 3. Get all items containing Vitamin A.

console.log(containsVitC(items, "Vitamin A"));

// 4. Group items based on the Vitamins that they contain in the following format:
//     {
//         "Vitamin C": ["Orange", "Mango"],
//         "Vitamin K": ["Mango"],
//     }
//     and so on for all items and all Vitamins.

function GroupeVitamin(data) {
    if (!Array.isArray(data)) {
        return [];
    }
    let GroupeOfVitamin = {};
    data.map((element) => {
        element.contains = element.contains.split(", ");
        element.contains.map((arrOfVit) => {
            if (!(arrOfVit in GroupeOfVitamin)) {
                GroupeOfVitamin[arrOfVit] = [];
            }
            GroupeOfVitamin[arrOfVit].push(element.name);
        });
    });
    return GroupeOfVitamin;
}
console.log(GroupeVitamin(items));

// 5. Sort items based on number of Vitamins they contain.

function sortItems(data) {
    if (!Array.isArray(data)) {
        return [];
    }
    function compare(value1, value2) {
        if (value1.contains.length > value2.contains.length) {
            return -1;
        }
        if (value1.contains.length < value2.contains.length) {
            return 1;
        }
        return 0;
    }
    return data.sort(compare);
}
console.log(sortItems(items));
