const products = [
    {
        shampoo: {
            price: "$50",
            quantity: 4,
        },
        "Hair-oil": {
            price: "$40",
            quantity: 2,
            sealed: true,
        },
        comb: {
            price: "$12",
            quantity: 1,
        },
        utensils: [
            {
                spoons: { quantity: 2, price: "$8" },
            },
            {
                glasses: { quantity: 1, price: "$70", type: "fragile" },
            },
            {
                cooker: { quantity: 4, price: "$900" },
            },
        ],
        watch: {
            price: "$800",
            quantity: 1,
            type: "fragile",
        },
    },
];

// Q1. Find all the items with price more than $65.

function itemsMoreThan65(data) {
    let res = [];
    res = Object.keys(data[0]).map((product) => {
        if (Array.isArray(data[0][product])) {
            let ans = [];
            data[0][product].map((items) => {
                Object.keys(items).map((item) => {
                    if (Number(items[item].price.replace("$", "")) > 65) {
                        ans.push(items[item]);
                        return;
                    }
                });
            });
            console.log("ans", ans);
        }
    });
    return res;
}

console.log(itemsMoreThan65(products));

// Q2. Find all the items where quantity ordered is more than 1.
// Q.3 Get all items which are mentioned as fragile.
// Q.4 Find the least and the most expensive item for a single quantity.
// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

// NOTE: Do not change the name of this file
